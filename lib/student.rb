class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(f_name, l_name)
    @first_name = f_name
    @last_name = l_name
    @courses = []
  end

  def name
    @first_name + ' ' + @last_name
  end

  def enroll(course)
    unless @courses.include?(course)
      @courses.each { |el| raise 'Schedule Conflict' if el.conflicts_with?(course) }
      courses << course
      course.students << self
    end
  end

  def course_load
    load_by_dept = Hash.new(0)
    @courses.each { |el| load_by_dept[el.department] += el.credits }
    load_by_dept
  end
end
